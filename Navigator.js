import { createAppContainer, createStackNavigator } from 'react-navigation'
import Example from "./screen/Example/Example";

const Navigator = createStackNavigator({
    Example
});

export default createAppContainer(Navigator)